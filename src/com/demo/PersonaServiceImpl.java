package com.demo;

import java.util.ArrayList;
import java.util.List;

public class PersonaServiceImpl implements IPersonaService {

	@Override
	public Persona buscarPorId(int id) {
		List<Persona> lista = new ArrayList<Persona>();
		lista.add(new Persona(1, "Juan"));
		lista.add(new Persona(2, "Luis"));
		
		Persona persona = null;
		for (Persona p : lista) {
			if(p.getId() == id) {
				persona = p;
			}
		}
		return persona;
	}

}
